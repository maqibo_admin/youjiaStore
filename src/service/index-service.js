/*
* @Author: Rosen
* @Date:   2017-05-17 18:55:04
* @Last Modified by:   Rosen
* @Last Modified time: 2017-06-02 17:51:15
*/

'use strict';

var _mm = require('util/mm.js');

var _index = {
    // 获取分类查询
    getCategoryList : function(resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/category/list.do'),
            method  : 'post',
            success : resolve,
            error   : reject
        });
    },
    //获取首页轮播图
    getIndexSwiper : function(resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/content/list.do'),
            method  : 'post',
            success : resolve,
            error   : reject
        });
    },
}
module.exports = _index;