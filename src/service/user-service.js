/*
* @Author: Rosen
* @Date:   2017-05-17 17:04:32
* @Last Modified by:   Rosen
* @Last Modified time: 2017-05-24 17:11:19
*/

'use strict';

var _mm = require('util/mm.js');

var _user = {
    // 用户登录
    login : function(userInfo, resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/user/login.do'),
            data    : userInfo,
            method  : 'POST',
            success : resolve,
            error   : reject
        });
    },
    // 检查用户名
    checkUsername : function(username, resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/user/check_valid.do'),
            data    : {
                type    : 'username',
                str     : username
            },
            method  : 'POST',
            success : resolve,
            error   : reject
        });
    },
    // 用户注册
    register : function(userInfo, resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/user/register.do'),
            data    : userInfo,
            method  : 'POST',
            success : resolve,
            error   : reject
        });
    },
    // 检查登录状态
    checkLogin : function(resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/user/get_user_info.do'),
            method  : 'POST',
            success : resolve,
            error   : reject
        });
    },
    // 获取用户密码提示问题
    getQuestion : function(username, resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/user/forget_get_question.do'),
            data    : {
                username : username
            },
            method  : 'POST',
            success : resolve,
            error   : reject
        });
    },
    // 检查密码提示问题答案
    checkAnswer : function(userInfo, resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/user/forget_check_answer.do'),
            data    : userInfo,
            method  : 'POST',
            success : resolve,
            error   : reject
        });
    },
    // 重置密码
    resetPassword : function(userInfo, resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/user/forget_reset_password.do'),
            data    : userInfo,
            method  : 'POST',
            success : resolve,
            error   : reject
        });
    },
    // 获取用户信息
    getUserInfo : function(resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/user/get_information.do'),
            method  : 'POST',
            success : resolve,
            error   : reject
        });
    },
    // 更新个人信息
    updateUserInfo : function(userInfo, resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/user/update_information.do'),
            data    : userInfo,
            method  : 'POST',
            success : resolve,
            error   : reject
        });
    },
    // 登录状态下更新密码
    updatePassword : function(userInfo, resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/user/reset_password.do'),
            data    : userInfo,
            method  : 'POST',
            success : resolve,
            error   : reject
        });
    },
    // 登出
    logout : function(resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/user/logout.do'),
            method  : 'POST',
            success : resolve,
            error   : reject
        });
    },
    //商户入驻
    shopRegister : function(formData,resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/shopInfo/register_shop.do'),
            method  : 'POST',
            data    : formData,
            success : resolve,
            error   : reject
        });
    },
    //检查登录用户是否注册过商铺
    getIsRegisterShop : function(resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/shopInfo/own_shop_state.do'),
            method  : 'POST',
            success : resolve,
            error   : reject
        });
    },
    //创建订单
    onCreateOrder : function(resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/deposit/order/createOrder.do'),
            method  : 'POST',
            success : resolve,
            error   : reject
        });
    },
    //商铺退款
    onShopRefund : function(resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/deposit/order/alipay_refund_deposit.do'),
            method  : 'POST',
            success : resolve,
            error   : reject
        });
    },
    //手机验证
    onPhoneSendMsg : function(phone,resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/user/get_update_code.do'),
            method  : 'POST',
            data    : {
                phone : phone
            },
            success : resolve,
            error   : reject
        });
    },
    //手机验证修改密码
    onPhoneUpdatePwd : function(data,resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/user/phone_reset_password.do'),
            method  : 'POST',
            data    : data,
            success : resolve,
            error   : reject
        });
    },
    //手机注册发送验证码
    onPhoneRegisterMsg : function(phone,resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/user/get_insert_code.do'),
            method  : 'POST',
            data    : {
                phone : phone
            },
            success : resolve,
            error   : reject
        });
    },
    //查询是否注册为商铺，是则回填
    onIsRegisterShop : function(resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/shopInfo/own_shop.do'),
            method  : 'POST',
            success : resolve,
            error   : reject
        });
    },
    //查询商铺
    queryShopDetail : function(id,resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/shopInfo/shop_detail.do'),
            method  : 'POST',
            data    :{
                shopUserId : id
            },
            success : resolve,
            error   : reject
        });
    }, 
}
module.exports = _user;