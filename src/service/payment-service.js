/*
* @Author: Rosen
* @Date:   2017-06-10 20:28:03
* @Last Modified by:   Rosen
* @Last Modified time: 2017-06-10 20:35:25
*/

'use strict';
var _mm = require('util/mm.js');

var _payment = {
    // 获取支付信息
    getPaymentInfo : function(orderNumber, resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/order/pay.do'),
            data    : {
                orderNo : orderNumber
            },
            success : resolve,
            error   : reject
        });
    },
    //获取商铺支付押金信息
    onPayGlod: function (orderNumber,resolve, reject) {
        _mm.request({
            url: _mm.getServerUrl('/deposit/order/alipay.do'),
            data : {
                orderNo : orderNumber
            },
            method: 'POST',
            success: resolve,
            error: reject
        });
    },
    // 获取商品订单状态
    getPaymentStatus : function(orderNumber, resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/order/query_order_pay_status.do'),
            data    : {
                orderNo : orderNumber
            },
            success : resolve,
            error   : reject
        });
    },
    // 获取押金订单状态
    getGlodStatus : function(orderNumber, resolve, reject){
        _mm.request({
            url     : _mm.getServerUrl('/deposit/order/alipay_query_order_pay_status.do'),
            data    : {
                orderNo : orderNumber
            },
            success : resolve,
            error   : reject
        });
    }
}
module.exports = _payment;