/*
* @Author: Rosen
* @Date:   2017-05-08 15:19:12
 * @Last Modified by: mikey.zhaopeng
 * @Last Modified time: 2018-04-22 13:40:00
*/

'use strict';
require('./index.css');
require('page/list/index.css');
require('page/common/nav/index.js');
require('page/common/header/index.js');
require('util/slider/index.js');
var navSide = require('page/common/nav-side/index.js');
var templateBanner = require('./banner.string');
var _mm = require('util/mm.js');
var _index = require('service/index-service.js');
var templateList = require('./list.string');
var templateFloor = require('./floor.string');
var _product = require('service/product-service.js')

var index = {
    init: function () {
        this.bind();
    },
    bind: function () {
        this.bannerSwiper();
        //主页列表
        this.getListPagination();
        //商品
        this.getProductList();
    },
    //轮播组件开启
    bannerSwiper: function () {
        _index.getIndexSwiper(function (res) {
            var bannerHtml = _mm.renderHtml(templateBanner,{list : res});
            $('.banner-con').html(bannerHtml);
            // 初始化banner
            var $slider = $('.banner').unslider({
                dots: true
            });
            // 前一张和后一张操作的事件绑定
            $('.banner-con .banner-arrow').click(function () {
                var forward = $(this).hasClass('prev') ? 'prev' : 'next';
                $slider.data('unslider')[forward]();
            });
        }, function (err) {
            _mm.errorTips(err);
        })
    },
    //列表分页获取
    getListPagination: function () {
        //获取主页轮播旁列表
        var that = this;
        _index.getCategoryList(function (res) {
            that.onRenderList(res);
        }, function (err) {
            _mm.errorTips(err);
        })
    },
    //渲染列表
    onRenderList: function (res) {
        var listHtml = _mm.renderHtml(templateList, { res: res.result });
        $('.keywords-list').html(listHtml)
    },
    //主页获取商品
    getProductList: function () {
        _product.getProductList({}, function (res) {
            var floor = _mm.renderHtml(templateFloor, { list: res.list });
            $('.p-list-con').html(floor);
        }, function (err) {
            _mm.errorTips(err);
        })
    }
}
module.exports = index.init();
