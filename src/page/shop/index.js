require('./index.css');
require('page/list/index.css');
require('page/common/nav/index.js');
require('page/common/header/shop-index.js')

var _user           = require('service/user-service.js')
var _mm             = require('util/mm.js');
var _product        = require('service/product-service.js');
var Pagination      = require('util/pagination/index.js');
var templateList    = require('page/index/floor.string');
var templateDetail  = require('./shop-detail.string');

var index = {
    data : {
        listParam : {
            pageNum : 1,
            pageSize: 10,
            shopUserId:_mm.getUrlParam('shopUserId'),
        }
    },
    init: function () {
        this.bind();
    },
    bind: function () {
        this.queryShop();
        
    },
    queryShop : function(){
        var that = this;
        if(_mm.getUrlParam('keyword')){
            that.data.listParam.keyword=_mm.getUrlParam('keyword');
        }
       _product.getProductList(this.data.listParam,function(res){
            if(!_mm.getUrlParam('keyword')){
                _mm.removeStorage('shopMsg');
                _mm.setStorage('shopMsg',{name:res.list[0].shopName,id:res.list[0].shopUserId})
            }
            if($('.shop-item .fl').length == 0){
                var name=_mm.getStorage('shopMsg').name;
            var html = '';
            html += "<div class='fl'><span class='label'>商铺名称：</span>   <span class='info'>"+name+"</span></div><div class='fr'><a href='javascript:;' id='open'>展开</a></div>"
            $('.shop-item').html(html);
            }
            //点击展开
            that.openShop();
            var floor = _mm.renderHtml(templateList, { list: res.list });
            $('.p-list-con').html(floor);
            that.loadPagination({
                hasPreviousPage : res.hasPreviousPage,
                prePage         : res.prePage,
                hasNextPage     : res.hasNextPage,
                nextPage        : res.nextPage,
                pageNum         : res.pageNum,
                pages           : res.pages
            });
       },function(err){
           _mm.errorTips(err)
       }) 
    },
    // 加载分页信息
    loadPagination : function(pageInfo){
        var _this = this;
        this.pagination ? '' : (this.pagination = new Pagination());
        this.pagination.render($.extend({}, pageInfo, {
            container : $('.pagination'),
            onSelectPage : function(pageNum){
                _this.data.listParam.pageNum = pageNum;
                _this.queryShop();
            }
        }));
    },
    openShop : function(){
        $("#open").click(function(){
            if( $('.shop-detail').css('height')=='40px'){
                $('.shop-detail').addClass('active')
                $(this).html('收回');
                var id = _mm.getStorage('shopMsg').id;
                _user.queryShopDetail(id, function (res) {
                    var html = _mm.renderHtml(templateDetail,{res:res})
                    $('.shop-msg').html(html)
                }, function (err) {
                    _mm.errorTips(err);
                })
            }else{
                $(this).html('展开');
                $('.shop-detail').removeClass('active');
            }           
        })
    }
}
module.exports = index.init();