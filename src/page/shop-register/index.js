/*
* @Author: Rosen
* @Date:   2017-05-22 09:08:57
* @Last Modified by:   Rosen
* @Last Modified time: 2017-05-23 23:30:23
*/

'use strict';
require('./index.css');
require('page/common/nav-simple/index.js');
var _user = require('service/user-service.js');
var _mm = require('util/mm.js');
var _address = require('service/address-service.js')
var _addressModel = require('page/order-confirm/address-modal.js')
var shopType = 0    //是否为个人商铺和公司商铺
var isOnImg = true  //是否正在提交中
var arrImg = []    //要上传的图片

// 表单里的错误提示
var formError = {
    show: function (errMsg) {
        $('.error-item').show().find('.err-msg').text(errMsg);
    },
    hide: function () {
        $('.error-item').hide().find('.err-msg').text('');
    }
};

// page 逻辑部分
var page = {
    init: function () {
        this.bindEvent();
        this.onIsRegisterShop();
    },
    bindEvent: function () {
        //加载地址
        this.loadProvinces();
        this.loadProvinces1();
        var _this = this;
        $('.user-title').on('click', '.item', function (e) {
            var e = $(e.target).index();
            shopType = e;
            $(this).addClass('active').siblings().removeClass('active');
            $('.user-box').children().eq(e).css('display', 'block').siblings().css('display', 'none');
        })
        $('.license').on('click', 'div i', function (e) {
            $(this).parent().remove();
        }),
            $('.license1').on('click', 'div i', function (e) {
                $(this).parent().remove();
            }),
            // 注册按钮的点击
            $('#submit').click(function () {
                _this.submit();
            });
        $('#submit1').click(function () {
            _this.submit();
        });
        // 如果按下回车，也进行提交
        $('.user-content').keyup(function (e) {
            // keyCode == 13 表示回车键
            if (e.keyCode === 13) {
                _this.submit();
            }
        });
        this.onFileChange();
    },
    //如果没通过，进行回填
    onIsRegisterShop: function () {
        _user.onIsRegisterShop(function (res) {
            if (res.state === 2) {
                if (res.shopType == 1) {
                    $('.user-box').children().eq(1).css('display', 'block').siblings().css('display', 'none')
                    $('.user-title').children().eq(1).addClass('active').siblings().removeClass('active');
                    shopType = 1;
                    $('.companyName').html(res.companyName);
                    $('.organizationCode').html(res.organizationCode);
                    $('.shopName1').val(res.shopName)
                    $('.site1').val(res.site);
                    $('.person1').val(res.person);
                    $('.personNumber1').val(res.personNumber);
                    $('.companyPhone1').val(res.companyPhone);
                    $('.bankLicence1').val(res.bankLicence);
                    $('.bankId1').val(res.bankId);
                    $('.alipayName1').val(res.alipayName);
                    $('.alipayId1').val(res.alipayId);
                    $('.licenseNumber1').val(res.licenseNumber);
                    $('.businessScope1').val(res.businessScope);
                    var license = res.license.split(',')
                    var html = '';
                    for (var i = 0; i < license.length; i++) {
                        html += "<div class='item'><img src=" + license[i] + " /><i class='close'></i></div>"
                    }
                    $('.license1').html(html);
                }else if(shopType===0){
                    $('.shopName').val(res.shopName)
                    $('.site').val(res.site);
                    $('.person').val(res.person);
                    $('.personNumber').val(res.personNumber);
                    $('.companyPhone').val(res.companyPhone);
                    $('.bankLicence').val(res.bankLicence);
                    $('.bankId').val(res.bankId);
                    $('.alipayName').val(res.alipayName);
                    $('.alipayId').val(res.alipayId);
                    $('.licenseNumber').val(res.licenseNumber);
                    $('.businessScope').val(res.businessScope);
                    var license = res.license.split(',')
                    var html = '';
                    for (var i = 0; i < license.length; i++) {
                        html += "<div class='item'><img src=" + license[i] + " /><i class='close'></i></div>"
                    }
                    $('.license').html(html);
                }
            }else if(res.state === 1){
                return ;
            }else{
                _mm.successTips('您的商铺正在审核中或者已拥有，请勿在此页面注册');
                window.location.href='./index.html'
            }
        }, function (err) {
            return 
        })
    },
    //图片上传
    onFileChange: function () {
        var that =this;
        $('.user-item').on('change', '#fileupload', function (e) {
            that.onfileImg('fileupload', $('.license'));
        });
        $('.user-item').on('change', '#fileupload1', function (e) {
            that.onfileImg('fileupload1', $('.license1'));
        });
    },
    onfileImg: function (file, a) {
        if (typeof FileReader == 'undefined') {
            alert("<p>你的浏览器不支持FileReader接口！</p>");
        }
        var fileImg = document.getElementById(file).files[0];
        if (fileImg && !/image\/\w+/.test(fileImg.type)) {
            alert('上传的不是图片');
            return false;
        }
        var formData = new FormData();
        formData.append('upload_file', fileImg);
        if (fileImg) {
            isOnImg = false;
        }
        $.ajax({
            url: '/manage/product/upload.do',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (res) {
                isOnImg = true;
                var url = res.data.url;
                a.append("<div class='item'><img src=" + url + " /><i class='close'></i></div>");
            },
            erroe: function (err) {
                _mm.errorTips(err);
            }
        })
    },
    //加载城市信息
    loadProvinces: function () {
        var that = this;
        _address.getProvinces(function (res) {
            var provincesHtml = _addressModel.getSelectOption(res);
            $('.receiver-province').html(provincesHtml);
            // 省份和城市的二级联动
            $('.receiver-province').change(function () {
                var provin = $(this).val();
                if(provin==""){
                    $('.receiver-city').html('<option>请选择</option>')
                    $('.receiver-county').html('<option>请选择</option>')
                }else{
                    that.loadCities(provin);
                }
            });
        }, function (rej) {
            _mm.errorTips(rej);
        })
    },
    //加载城市信息
    loadCities: function (pid) {
        var that = this;
        _address.getCities(pid, function (res) {
            var provincesHtml = _addressModel.getSelectOption(res);
            $('.receiver-city').html(provincesHtml);
            $('.receiver-city').change(function () {
                var provin = $(this).val();
                if(provin==""){
                    $('.receiver-county').html('<option>请选择</option>')
                }else{
                    that.loadCounties(provin);
                }
            });
        }, function (err) {
            _mm.errorTips(err);
        })
    },
    //加载县级信息
    loadCounties: function (cid) {
        var that = this;
        _address.getCounties(cid, function (res) {
            var provincesHtml = _addressModel.getSelectOption(res);
            $('.receiver-county').html(provincesHtml);
        }, function (err) {
            _mm.errorTips(err);
        })
    },
    //加载城市信息
    loadProvinces1: function () {
        var that = this;
        _address.getProvinces(function (res) {
            var provincesHtml = _addressModel.getSelectOption(res);
            $('.receiver-province1').html(provincesHtml);
            // 省份和城市的二级联动
            $('.receiver-province1').change(function () {
                var provin = $(this).val();
                if(provin==""){
                    $('.receiver-city1').html('<option>请选择</option>')
                    $('.receiver-county1').html('<option>请选择</option>')
                }else{
                    that.loadCities1(provin);
                }
            });
        }, function (rej) {
            _mm.errorTips(rej);
        })
    },
    //加载城市信息
    loadCities1: function (pid) {
        var that = this;
        _address.getCities(pid, function (res) {
            var provincesHtml = _addressModel.getSelectOption(res);
            $('.receiver-city1').html(provincesHtml);
            $('.receiver-city1').change(function () {
                var provin = $(this).val();
                if(provin==""){
                    $('.receiver-county1').html('<option>请选择</option>')
                }else{
                    that.loadCounties1(provin);
                }
            });
        }, function (err) {
            _mm.errorTips(err);
        })
    },
    //加载县级信息
    loadCounties1: function (cid) {
        var that = this;
        _address.getCounties(cid, function (res) {
            var provincesHtml = _addressModel.getSelectOption(res);
            $('.receiver-county1').html(provincesHtml);
        }, function (err) {
            _mm.errorTips(err);
        })
    },
    // 提交表单
    submit: function () {
        var formData = {}
        if (shopType == 0) {
            arrImg = [];
            var license = $('.license').children();
            for (var i = 0; i < license.length; i++) {
                if (arrImg.indexOf($(license[i]).children().eq(0).attr('src')) == -1) {
                    arrImg.push($(license[i]).children().eq(0).attr('src'));
                }
            }
            formData = {
                shopName: $.trim($('.shopName').val()),
                shopType: shopType,
                province: $.trim($('.receiver-province option:selected').text()),
                city: $.trim($('.receiver-city option:selected').text()),
                county: $.trim($('.receiver-county option:selected').text()),
                areaId: $.trim($('.receiver-county').val()),
                areaName: $.trim($('.receiver-county option:selected').text()),
                site: $.trim($('.site').val()),
                person: $.trim($('.person').val()),
                personNumber: $.trim($('.personNumber').val()),
                companyPhone: $.trim($('.companyPhone').val()),
                bankLicence: $.trim($('.bankLicence').val()),
                bankId: $.trim($('.bankId').val()),
                alipayName: $.trim($('.alipayName').val()),
                alipayId: $.trim($('.alipayId').val()),
                license: arrImg.toString(),
                licenseNumber: $.trim($('.licenseNumber').val()),
                businessScope: $.trim($('.businessScope').val()),
                isOnImg: isOnImg
            }
        } else if (shopType == 1) {
            arrImg = [];
            var license = $('.license1').children();
            for (var i = 0; i < license.length; i++) {
                if (arrImg.indexOf($(license[i]).children().eq(0).attr('src')) == -1) {
                    arrImg.push($(license[i]).children().eq(0).attr('src'));
                }
            }
            formData = {
                companyName: $.trim($('.companyName').val()),
                shopType: shopType,
                organizationCode: $.trim($('.organizationCode').val()),
                shopName: $.trim($('.shopName1').val()),
                province: $.trim($('.receiver-province1').val()),
                city: $.trim($('.receiver-city1').val()),
                county: $.trim($('.receiver-county1').val()),
                areaId: $.trim($('.receiver-county1').val()),
                areaName: $.trim($('.receiver-county1 option:selected').text()),
                site: $.trim($('.site1').val()),
                person: $.trim($('.person1').val()),
                personNumber: $.trim($('.personNumber1').val()),
                companyPhone: $.trim($('.companyPhone1').val()),
                bankLicence: $.trim($('.bankLicence1').val()),
                bankId: $.trim($('.bankId1').val()),
                alipayName: $.trim($('.alipayName1').val()),
                alipayId: $.trim($('.alipayId1').val()),
                license: arrImg.toString(),
                licenseNumber: $.trim($('.licenseNumber1').val()),
                businessScope: $.trim($('.businessScope1').val()),
                isOnImg: isOnImg
            }
        }
        // 表单验证结果
        var validateResult = this.formValidate(formData);
        // 验证成功
        if (validateResult.status) {
            formError.hide();
            delete formData.isOnImg;
            _user.shopRegister(formData, function (res) {
                _mm.successTips('注册成功，正在审核中');
            }, function (errMsg) {
                formError.show(errMsg);
            });
        }
        // 验证失败
        else {
            // 错误提示
            formError.show(validateResult.msg);
        }

    },
    // 表单字段的验证
    formValidate: function (formData) {
        var result = {
            status: false,
            msg: ''
        };
        //看看是否有公司店铺
        if (formData.companyName == '') {
            result.msg = '公司名称不能为空';
            return result;
        }
        //公司证件号
        if (formData.organizationCode == '') {
            result.msg = '组织机构证件号不能为空';
            return result;
        }
        // 验证商铺名称是否为空
        if (!_mm.validate(formData.shopName, 'require')) {
            result.msg = '商铺名称不能为空';
            return result;
        }
        // 验证商铺名称是否为空
        if (formData.shopName.length > 16) {
            result.msg = '商铺名称不能大于16位';
            return result;
        }
        // 验证省份是否为空
        if (!_mm.validate(formData.province, 'require')) {
            result.msg = '省份不能为空';
            return result;
        }
        // 验证城市
        if (!_mm.validate(formData.city, 'require')) {
            result.msg = '城市不能为空';
            return result;
        }
        // 验证县级
        if (!_mm.validate(formData.county, 'require')) {
            result.msg = '县级不能为空';
            return result;
        }
        // 验证详细地址
        if (!_mm.validate(formData.site, 'require')) {
            result.msg = '详细地址不能为空';
            return result;
        }
        // 验证法人姓名
        if (!_mm.validate(formData.person, 'require')) {
            result.msg = '法人姓名不能为空';
            return result;
        }
        if (!_mm.validate(formData.personNumber, 'require')) {
            result.msg = '法人证件不能为空';
            return result;
        }
        if (!_mm.validate(formData.companyPhone, 'require')) {
            result.msg = '公司电话不能为空';
            return result;
        }
        if (!_mm.validate(formData.bankLicence, 'require')) {
            result.msg = '银行许可证件号不能为空';
            return result;
        }
        if (!_mm.validate(formData.bankId, 'require')) {
            result.msg = '银行卡号不能为空';
            return result;
        }
        if (!_mm.validate(formData.alipayName, 'require')) {
            result.msg = '支付宝姓名不能为空';
            return result;
        }
        if (!_mm.validate(formData.alipayId, 'require')) {
            result.msg = '支付宝账号不能为空';
            return result;
        }
        if (!formData.license && formData.isOnImg) {
            result.msg = '请上传执照图片';
            return result;
        } else if (!formData.license && !formData.isOnImg) {
            result.msg = '正在上传中，请稍等';
            return result;
        } else if (formData.license && !formData.isOnImg) {
            result.msg = '正在上传中，请稍等';
            return result;
        }
        if (!_mm.validate(formData.licenseNumber, 'require')) {
            result.msg = '营业执照注册号不能为空';
            return result;
        }
        if (!_mm.validate(formData.businessScope, 'require')) {
            result.msg = '经营范围不能为空';
            return result;
        }
        if (formData.businessScope.length > 100) {
            result.msg = '经营范围字数不能超过100';
            return result;
        }
        // 通过验证，返回正确提示
        result.status = true;
        result.msg = '验证通过';
        return result;
    }
};
$(function () {
    page.init();
});