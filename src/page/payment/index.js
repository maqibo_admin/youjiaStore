/*
* @Author: Rosen
* @Date:   2017-06-10 20:05:19
* @Last Modified by:   Rosen
* @Last Modified time: 2017-06-10 20:35:47
*/

'use strict';

require('./index.css');
require('page/common/nav/index.js');
require('page/common/header/index.js');
var _mm             = require('util/mm.js');
var _payment        = require('service/payment-service.js');
var templateIndex   = require('./index.string');

// page 逻辑部分
var page = {
    data: {
        orderNumber : _mm.getUrlParam('orderNumber'),
        orderNo     : _mm.getUrlParam('orderNo')
    },
    init: function(){
        this.onLoad();
    },
    onLoad : function(){
        // 加载detail数据
        if(this.data.orderNumber){
            this.loadPaymentInfo();
        }else{
            this.loadPaymentInfo1();
        }
    },
    // 加载商品订单列表
    loadPaymentInfo: function(){
        var _this           = this,
            paymentHtml     = '',
            $pageWrap       = $('.page-wrap');
        $pageWrap.html('<div class="loading"></div>');
        _payment.getPaymentInfo(this.data.orderNumber, function(res){
            // 渲染html
            paymentHtml = _mm.renderHtml(templateIndex, res);
            $pageWrap.html(paymentHtml);
            _this.listenOrderStatus();
        }, function(errMsg){
            $pageWrap.html('<p class="err-tip">' + errMsg + '</p>');
        });
    },
    // 加载商铺订单列表
    loadPaymentInfo1: function(){
        var _this           = this,
            paymentHtml     = '',
            $pageWrap       = $('.page-wrap');
        $pageWrap.html('<div class="loading"></div>');
        _payment.onPayGlod(this.data.orderNo, function(res){
            // 渲染html
            paymentHtml = _mm.renderHtml(templateIndex, res);
            $pageWrap.html(paymentHtml);
            _this.listenOrderStatus1();
        }, function(errMsg){
            $pageWrap.html('<p class="err-tip">' + errMsg + '</p>');
        });
    },
    // 监听商品订单状态
    listenOrderStatus : function(){
        var _this = this;
        this.paymentTimer = window.setInterval(function(){
            _payment.getPaymentStatus(_this.data.orderNumber, function(res){
                if(res == true){
                    window.location.href 
                        = './result.html?type=payment&orderNumber=' + _this.data.orderNumber;
                }
            });
        }, 5e3);
    },
    // 监听押金订单状态
    listenOrderStatus1 : function(){
        var _this = this;
        this.paymentTimer = window.setInterval(function(){
            _payment.getGlodStatus(_this.data.orderNo, function(res){
                if(res == true){
                    window.location.href 
                        = './result.html?type=shopPay'
                }
            });
        }, 5e3);
    }
};
$(function(){
    page.init();
});