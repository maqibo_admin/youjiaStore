/*
* @Author: Rosen
* @Date:   2017-05-23 19:33:33
 * @Last Modified by: mikey.zhaopeng
 * @Last Modified time: 2018-04-23 10:03:31
*/

'use strict';
require('./index.css');
require('page/common/nav/index.js');
require('page/common/header/index.js');
require('page/order-confirm/index.js')
var navSide         = require('page/common/nav-side/index.js');
var _mm             = require('util/mm.js');
var _user           = require('service/user-service.js');
var _address        = require('service/address-service.js');
var orderConfirm    = require('page/order-confirm/index.js');


// page 逻辑部分
var page = {
    init: function(){
        this.onLoad();
    },
    onLoad : function(){
        // 初始化左侧菜单
        navSide.init({
            name: 'shop-enter'
        });
        //加载商铺信息
        this.getIsRegisterShop();
    },
    //获取当前用户是否注册过商铺
    getIsRegisterShop : function(){
        var that = this;
        _user.getIsRegisterShop(function(res){
            if(res==0){$('.panel-body').html('您当前还未审核')}
            else if(res==1){                
                $('.panel-body').html("<p>您当前审核通过未交保证金</p><button class='onSubGold btn'>提交保证金</button>")
                //点击提交押金事件   
                $('.onSubGold').click(function(){
                    that.onPayMoney();
                })
            }
            else if(res==2){$('.panel-body').html('您当前审核未通过,请再次注册 <a href="./shop-register.html" target="_blank">点击注册</a>')}
            else if(res==3){
                $('.panel-body').html("<p>您当前审核通过已交保证金  <a href='http://admin.youja.shop' target='_blank'>点击登录</a></p><button class='refund btn'>退款</button>")
                //点击提交退款事件   
                $('.refund').click(function(){
                    that.onRefundMoney();
                })
            }
        },function(err){
            $('.panel-body').html(err)
        })
    },
    //创建订单，跳转交付押金页面
    onPayMoney:function(){
        _user.onCreateOrder(function(res){
            window.location.href = './payment.html?orderNo=' + res.orderNo;
        },function(err){
            _mm.errorTips(err);
        })
    },
    //退款
    onRefundMoney:function(){
        _user.onShopRefund(function(res){
            window.location.reload();
        },function(err){
            _mm.errorTips(err);
        })
    }
};
$(function(){
    page.init();
});