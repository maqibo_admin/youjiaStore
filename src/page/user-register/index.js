/*
* @Author: Rosen
* @Date:   2017-05-22 09:08:57
* @Last Modified by:   Rosen
* @Last Modified time: 2017-05-23 23:30:23
*/

'use strict';
require('./index.css');
require('page/common/nav-simple/index.js');
var _user   = require('service/user-service.js');
var _mm     = require('util/mm.js');
var isSend  = false;

// 表单里的错误提示
var formError = {
    show : function(errMsg){
        $('.error-item').show().find('.err-msg').text(errMsg);
    },
    hide : function(){
        $('.error-item').hide().find('.err-msg').text('');
    }
};

// page 逻辑部分
var page = {
    init: function(){
        this.bindEvent();
    },
    bindEvent : function(){
        var _this = this;
        // 验证username
        $('#username').blur(function(){
            var username = $.trim($(this).val());
            // 如果用户名为空，我们不做验证
            if(!username){
                return;
            }
            // 异步验证用户名是否存在
            _user.checkUsername(username, function(res){
                formError.hide();
            }, function(errMsg){
                formError.show(errMsg);
            });
        });
        // 注册按钮的点击
        $('#submit').click(function(){
            _this.submit();
        });
        // 如果按下回车，也进行提交
        $('.user-content').keyup(function(e){
            // keyCode == 13 表示回车键
            if(e.keyCode === 13){
                _this.submit();
            }
        });
        $('.inputCode').click(function(e){
            var  phone = $.trim($('#phone').val());
            var  inputCode     = $(e.target);
            var reg = /^[1][3,4,5,6,7,8][0-9]{9}$/; 
            if(reg.test(phone)){
                if(isSend){
                    return
                }else{
                    isSend = true;
                    inputCode.html("正在获取中");
                    _user.onPhoneRegisterMsg(phone,function(res){
                        var time = 59 ;
                        var timer = setInterval(function(e){
                            inputCode.html(""+time+"s后获取");
                            if(time == 0){
                                clearInterval(timer);
                                isSend = false;
                                inputCode.html("获取验证码");
                            }
                            time--;
                        },1000)
                    },function(err){
                        isSend = false;
                        inputCode.html("获取验证码");
                        _mm.errorTips(err);
                    })
                }
            }else{
                _mm.errorTips('您输入的号码不符合规则');
            }
        })
    },
    // 提交表单
    submit : function(){
        var formData = {
                username        : $.trim($('#username').val()),
                password        : $.trim($('#password').val()),
                passwordConfirm : $.trim($('#password-confirm').val()),
                phone           : $.trim($('#phone').val()),
                code            : $.trim($('#code').val()),
            },
            // 表单验证结果
            validateResult = this.formValidate(formData);
        // 验证成功
        if(validateResult.status){
            _user.register(formData, function(res){
                window.location.href = './result.html?type=register';
            }, function(errMsg){
                formError.show(errMsg);
            });
        }
        // 验证失败
        else{
            // 错误提示
            _mm.errorTips(validateResult.msg);
            formError.show(validateResult.msg);
        }

    },
    // 表单字段的验证
    formValidate : function(formData){
        var result = {
            status  : false,
            msg     : ''
        };
        // 验证用户名是否为空
        if(!_mm.validate(formData.username, 'require')){
            result.msg = '用户名不能为空';
            return result;
        }
        // 验证用户名是否为空
        if(!_mm.validate(formData.username, 'username')){
            result.msg = '用户名以字母开头，且长度大于6位小于15位';
            return result;
        }
        // 验证密码是否为空
        if(!_mm.validate(formData.password, 'require')){
            result.msg = '密码不能为空';
            return result;
        }
        // 验证密码长度
        if(formData.password.length < 6 || formData.password.length > 18){
            result.msg = '密码长度不能少于6位且不能大于18位';
            return result;
        }
        // 验证两次输入的密码是否一致
        if(formData.password !== formData.passwordConfirm){
            result.msg = '两次输入的密码不一致';
            return result;
        }
        // 验证手机号
        if(!_mm.validate(formData.phone, 'phone')){
            result.msg = '手机号格式不正确';
            return result;
        }
        // 验证 手机验证码
        if(!_mm.validate(formData.code, 'require')){
            result.msg = '验证码不能为空';
            return result;
        }
        // 通过验证，返回正确提示
        result.status   = true;
        result.msg      = '验证通过';
        return result;
    }
};
$(function(){
    page.init();
});