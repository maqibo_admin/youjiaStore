'use strict';
require('../user-pass-reset/index.css');
require('./index.css');
require('page/common/nav-simple/index.js');
var _user   = require('service/user-service.js');
var _mm     = require('util/mm.js');
var isSend  = false;

var page = {
    init: function(){
        this.bindEvent();
    },
    bindEvent : function(){
        this.onSendMsg();
    },
    onSendMsg:function(){
        var that = this;
        //点击验证手机号码
        $('.inputCode').click(function(e){
            var  phone = $.trim($('#phone').val());
            var  inputCode     = $(e.target);
            var reg = /^[1][3,4,5,6,7,8][0-9]{9}$/; 
            if(reg.test(phone)){
                if(isSend){
                    return
                }else{
                    isSend = true;
                    inputCode.html("正在获取中");
                    _user.onPhoneSendMsg(phone,function(res){
                        var time = 59 ;
                        var timer = setInterval(function(e){
                            inputCode.html(""+time+"s后获取");
                            if(time == 0){
                                clearInterval(timer);
                                isSend = false;
                                inputCode.html("请获取验证码");
                            }
                            time--;
                        },1000)
                    },function(err){
                        isSend = false;
                        inputCode.html("获取验证码");
                        _mm.errorTips(err);
                    })
                }
            }else{
                _mm.errorTips('您输入的号码不符合规则');
            }
        })
        //点击进行提交
        $('.btn-block').click(function(e){
            var formData = {
                phone       : $.trim($('#phone').val()),
                code        : $.trim($('#code').val()),
                passwordNew : $.trim($('#pwd1').val()),
                password    : $.trim($('#pwd2').val()),
            }
            var state = that.formValidate(formData);
            if(state.status){
                delete formData.password;
                _user.onPhoneUpdatePwd(formData,function(res){
                    _mm.successTips(res);
                    window.location.href='./user-login.html';
                },function(err){
                    _mm.errorTips(err);
                })
            }else{
                _mm.errorTips(state.msg)
            }
        })
        //点击返回上一页
        $('.con-left').click(function(e){
            window.history.go(-1);
        })
    },
    //验证字符串什么的
    formValidate : function(formData){
        var result = {
            status  : false,
            msg     : ''
        };
        if(!_mm.validate(formData.phone, 'require')){
            result.msg = '手机号不能为空';
            return result;
        }
        if(!_mm.validate(formData.phone, 'phone')){
            result.msg = '手机号格式错误';
            return result;
        }
        if(!_mm.validate(formData.code, 'require')){
            result.msg = '验证码不能为空';
            return result;
        }
        if(!_mm.validate(formData.passwordNew, 'require')){
            result.msg = '新密码不能为空';
            return result;
        }
        if(formData.passwordNew.length<6 || formData.passwordNew.length>18){
            result.msg = '新密码不能小于6位大于18位';
            return result;
        }
        if(!_mm.validate(formData.password, 'require')){
            result.msg = '请再次输入新密码';
            return result;
        }
        if(formData.passwordNew !== formData.password){
            result.msg = '两次密码输入不一致';
            return result;
        }
        result.status   = true;
        result.msg      = '验证通过';
        return result;
    }
};
$(function(){
    page.init();
});